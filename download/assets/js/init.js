 (function($) {
     
    'use strict';
    
    /**
    * =====================================
    * Preloader     
    * =====================================
    */  

	var loaderAnimation = $("#html5Loader").LoaderAnimation({
		onComplete:function(){
			console.log("preloader animation completed!");
		}
	});
	$.html5Loader({
			filesToLoad:'files.json',
			onComplete: function () {
				console.log("All the assets are loaded!");
			},
			onUpdate: loaderAnimation.update
	});

   jQuery(document).on('ready', function () {
       
            
        /**
        * =====================================
        * Function for windows height and width      
        * =====================================
        */    
            
            if ($(window).width() < 768) {
                $.scrollify.disable();
            } else{
                $.scrollify.enable();
            }
            
        /**
        * =====================================
        * Scrolling     
        * =====================================
        */
            
            $.scrollify({
        		section : "section"
        	});
        	
            $('body').scrollspy({ target: '#nav' })
            
            $('a[href*="#"]:not([href="#"])').click(function() {
                if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                  var target = $(this.hash);
                  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                  if (target.length) {
                    $('html, body').animate({
                      scrollTop: target.offset().top
                    }, 1000);
                    return false;
                  }
                }
            });
            
        /**
        * =====================================
        * Navigation     
        * =====================================
        */
            
            $(".navbar-toggle").on("click", function () {
        		$(this).toggleClass("active");
        	});
          
            $('#nav').localScroll({
                target:'body'
            });
        	
            $('ul#main-menu li a').click(function (e) {
                $('ul#main-menu li.active').removeClass('active')
                $(this).parent('li').addClass('active')
            })
            
            
            $('#nav-ul-testimonial a').on('click', function() {
                $(this).tab('show');
            });
            
        /**
        * =====================================
        * Steller      
        * =====================================
        */  
            $.stellar({
                horizontalScrolling: false
            });
            
            
            $('.scene').parallax();
            
        /**
        * =====================================
        * Animation      
        * =====================================
        */
            
            new WOW().init();
        
        /**
        * =====================================
        * Portfolio     
        * =====================================
        */
        
            $('.view-project-detail').on('click', function(event) {
                event.preventDefault();
                var href          = $(this).attr('href') + ' ' + $(this).attr('data-action'),
                    dataShow      = $('#project-gallery-view'),
                    dataShowMeta  = $('#project-gallery-view meta'),
                    dataHide      = $('#project-gallery'),
                    preLoader     = $('#loader'),
                    backBtn       = $('#back-button'),
                    filterBtn     = $('#filter-button');
        
                    dataHide.animate( { 'marginLeft':'-120%' }, { duration: 400, queue: false } );
                    filterBtn.animate( { 'marginLeft':'-120%' }, { duration: 400, queue: false } );
                    dataHide.fadeOut(400);
                    filterBtn.fadeOut(400);
                    setTimeout( function() { preLoader.show(); }, 400);
                    setTimeout( function() {
                        dataShow.load( href, function() {
                        dataShowMeta.remove();
                        preLoader.hide();
                        dataShow.fadeIn(600);
                        backBtn.fadeIn(600);
                      });
                },800);
            });
        
            $('#back-button').on('click', function(event) {
              event.preventDefault();
              var dataShow    = $('#project-gallery'),
                  dataHide    = $('#project-gallery-view'),
                  filterBtn   = $('#filter-button');
            
              $("[data-animate]").each( function() {
                  $(this).addClass($(this).attr('data-animate'));
              });
            
              dataHide.fadeOut(400);
              $(this).fadeOut(400);
              setTimeout(function(){
                  dataShow.animate( { 'marginLeft': '0' }, { duration: 400, queue: false } );
                  filterBtn.animate( { 'marginLeft': '0' }, { duration: 400, queue: false } );
                  dataShow.fadeIn(400);
                  filterBtn.fadeIn(400);
              },400);
              setTimeout(function(){
                  dataShow.find('.fadeInRight, .fadeInLeft, .fadeInUp, .fadeInDown').removeClass('fadeInRight').removeClass('fadeInLeft').removeClass('fadeInUp').removeClass('fadeInDown');
              },1500);
            });
            
            
        /**
        * =====================================
        *  CONTACT SECTION SAME HEIGHT
        * =====================================
        */ 
        
        var height = Math.max($("#left").height(), $("#right").height());
        $("#left").height(height);
        $("#right").height(height);
        
        
        /**
        * =====================================
        * CONTACT FORM
        * =====================================
        */

        $("#contactForm").validator().on("submit", function (event) {
            if (event.isDefaultPrevented()) {
              // handle the invalid form...
              formError();
              submitMSG(false, "Did you fill in the form properly?");
            } else {
              // everything looks good!
              event.preventDefault();
              submitForm();
            }
        });
    
        function submitForm(){
          var name = $("#name").val();
          var email = $("#email").val();
          var message = $("#message").val();
          $.ajax({
              type: "POST",
              url: "process.php",
              data: "name=" + name + "&email=" + email + "&message=" + message,
              success : function(text){
                  if (text == "success"){
                      formSuccess();
                    } else {
                      formError();
                      submitMSG(false,text);
                    }
                }
            });
        }
        function formSuccess(){
            $("#contactForm")[0].reset();
            submitMSG(true, "Message Sent!")
        }
    	function formError(){
    	    $("#contactForm").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
    	        $(this).removeClass();
    	    });
    	}
        function submitMSG(valid, msg){
            if(valid){
              var msgClasses = "h3 text-center fadeInUp animated text-success";
            } else {
              var msgClasses = "h3 text-center shake animated text-danger";
            }
            $("#msgSubmit").removeClass().addClass(msgClasses).text(msg);
        }
            
    });
    
    
    /**
    * =====================================
    * ISOTOPE    
    * =====================================
    */
    
    
    $(window).load(function(){
        var $container = $('.portfolioContainer');
        $container.isotope({
            filter: '*',
            animationOptions: {
                queue: true
            }
        });
     
        $('.portfolio-nav li').click(function(){
            $('.portfolio-nav .current').removeClass('current');
            $(this).addClass('current');
     
            var selector = $(this).attr('data-filter');
            $container.isotope({
                filter: selector,
                animationOptions: {
                    queue: true
                }
             });
             return false;
        }); 
    });
    

    /**
    * =====================================
    * Map     
    * =====================================
    */
    
    google.maps.event.addDomListener(window, 'load', init);
    function init() {
        var mapOptions = {
          zoom: 17,
          scrollwheel: false, 
          navigationControl: false,
          center: new google.maps.LatLng(24.906308,91.870413), // Change Map Location
          styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},
          {"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},
          {"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},
          {"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},
          {"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},
          {"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},
          {"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},
          {"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},
          {"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},
          {"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},
          {"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},
          {"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},
          {"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
      };
      var mapElement = document.getElementById('map');
      var map = new google.maps.Map(mapElement, mapOptions);
      var marker = new google.maps.Marker({
          position: new google.maps.LatLng(24.906308,91.870413),
          map: map,
          title: '24 Golden Tower (2nd floor), Amborkhana, Sylhet.!'
      });
    }
    

} (jQuery) );
